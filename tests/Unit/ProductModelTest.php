<?php
use App\Models\Product;

// test('productmodel', function () {
//     expect(true)->toBeTrue();
// });

it("test name attribute should be camel case", function () {
    $product = new Product(
        [
            "name"=> "product 1",
            "price"=> 100
        ]
    );

    expect($product->name)->toBe("Product 1");
    expect($product->price)->toBe(100);
});
