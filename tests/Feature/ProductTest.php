<?php
use function Pest\Laravel\postJson as pJ;

it('has product page', function () {
    $response = $this->get('/api/product');

    $response->assertStatus(200);
});

it('can create a product', function () {
    $data = [
        'name'=> 'Product 1',
        'price'=> 100
    ];

    pJ('/api/product', $data)->assertStatus(201);
});
